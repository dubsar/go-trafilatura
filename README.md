# go trafilatura

## acknowledgements

this is just a remainder on how to build the excellent package [go-trafilatura](https://github.com/markusmobius/go-trafilatura).

## wasm

the wasm folder creates a service that deno launches but crashes as soon as it is called because trafilatura is as fast as memory greedy.

kept here just as a template on how to integrate a go wasm artefact in a deno service.
