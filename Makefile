PWD=$(shell pwd)
UID=$(shell id -u)
GID=$(shell id -g)
BIN=./bin
EXE=$(BIN)/trafilatura

docker:
	mkdir -p $(BIN)
	docker run --rm -it -v $(PWD):/node -w /node golang:1.20 make exe
	sudo chown $(UID):$(GID) $(EXE)
	sudo rm go.*

exe:
	go mod init github.com/trafilatura
	go install github.com/markusmobius/go-trafilatura/cmd/go-trafilatura@latest
	cp /go/bin/go-trafilatura $(EXE)

.PHONY: docker exe
