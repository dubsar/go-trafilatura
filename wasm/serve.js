import { serve } from "https://deno.land/std@0.177.0/http/server.ts";
import { Go } from "./lib/wasm_exec.js";

let go = new Go();
let result = await WebAssembly.instantiate(Deno.readFileSync("./bin/trafilatura.wasm"), go.importObject);
go.run(result.instance);


serve((_) => {
  const text = extract("https://hub.docker.com/_/golang");
  console.info("text: ", text);
  return new Response(text)
});
