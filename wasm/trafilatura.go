package main

import (
	"fmt"
	"net/http"
	nurl "net/url"
	"regexp"
	"time"
	"syscall/js"

	//"github.com/go-shiori/dom"
	"github.com/markusmobius/go-trafilatura"
	//"github.com/sirupsen/logrus"
)

var (
	httpClient = &http.Client{Timeout: 30 * time.Second}
	rxCharset  = regexp.MustCompile(`(?i)charset\s*=\s*([^;\s"]+)`)
)

func extract(url string) (string, error) {
	parsedURL, err := nurl.ParseRequestURI(url)
	if err != nil {
		return "", err
	}

	resp, err := httpClient.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	opts := trafilatura.Options{
		IncludeImages: true,
		OriginalURL:   parsedURL,
	}

	result, err := trafilatura.Extract(resp.Body, opts)
	if err != nil {
		return "", err
	}

	return string(result.ContentText), nil
}

func jsonWrapper() js.Func {  
	jsonFunc := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
					if len(args) != 1 {
									return "Invalid no of arguments passed"
					}
					url := args[0].String()
					text, err := extract(url)
					if err != nil {
									fmt.Printf("unable to extract %s\n", err)
									return err.Error()
					}
					return text
	})
	return jsonFunc
}

func main() {  
	c := make(chan struct{})
	js.Global().Set("extract", jsonWrapper())
	<-c
}
